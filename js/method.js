let domID = (id) => {
  return document.getElementById(id);
};
let domSelec = (id) => {
  return document.querySelector(id);
};

let informationChange = (viTriGlass) => {
  return `<div style="color : white"><p> ${viTriGlass.name} - ${viTriGlass.brand} (${viTriGlass.color})</p> 
  <span style="padding : 10px 5px; background : red">${viTriGlass.price} $</span>
  <p style=" margin: 20px 0px">${viTriGlass.description}</p></div>
  `;
};
let imgChange = (viTriGlass) => {
  return `<img src="${viTriGlass.virtualImg}" alt="" />`;
};
export { domID, domSelec, informationChange, imgChange };
